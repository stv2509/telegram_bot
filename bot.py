#/usr/bin/python

import telebot
import subprocess
import time
import re
from netmiko import ConnectHandler


bot = telebot.TeleBot('833877163:AAEixSirxe-JiagHGFGY3ZQLQQ387CPcEqE');

@bot.message_handler(content_types=['text'])


def get_text_messages(message):
    
    #####################################################################################################
    def get_ipaddr():
        try:
          IPAddr = re.search("\d+\.\d+\.\d+\.\d+",message.text.strip()).group()
          return IPAddr
        except:
            bot.send_message(message.from_user.id, "Некорректный ip-адрес. Попробуйте еще раз.")
    #####################################################################################################


    #####################################################################################################
    def ping_device():
        IPAddr = get_ipaddr()
        try:
            process = subprocess.Popen("ping %s -n 3" % IPAddr, stdout=subprocess.PIPE, universal_newlines=True)
            data_1 = (str(process.communicate())).encode('1251').decode('cp866').replace("\\n", " ").replace("ad", "н")
            data = re.sub('[\\\\x]|\B0\B', '', data_1)
            bot.send_message(message.from_user.id, data)
        except:
            bot.send_message(message.from_user.id, "Ошибка!!!")
    #####################################################################################################

    #####################################################################################################
    def wifi_reboot():
        user = b"admin"
        IPAddr = get_ipaddr()
        try:
            passwords = [b'AQ9xLtSXrI', b'JR17rw9XOk', b'QNf2nskcee', b'RyYcId0ouO', b'yjIZ3XPHGM']
            result = subprocess.call("ping %s -n 3" % IPAddr, stdout=subprocess.PIPE, universal_newlines=True)
            if result==0:
                for secret in passwords:
                    try:
                        DEVICE_PARAMS = {'device_type': 'linux',
                                         'ip': IPAddr,
                                         'username': user,
                                         'password': secret,
                                         'timeout': 150,
                                         'global_delay_factor': 3
                                         }

                        ssh = ConnectHandler(**DEVICE_PARAMS)
                        bot.send_message(message.from_user.id, "Учетные данные верные. Перезагружаю антенну")
                        ssh.send_command_timing("reboot %s" % IPAddr, delay_factor=3)
                        ping_device()
                    except:
                        bot.send_message(message.from_user.id, "Не правильный логин/пароль, пробую следующий")
        except:
            bot.send_message(message.from_user.id, "Устройство не доступно!")
    #####################################################################################################


    

    if message.text == "/help" or message.text == None:
        bot.send_message(message.from_user.id, "Примеры:\n\n"
                                               "ping 1.1.1.1\n"
                                               "reboot 1.1.1.1\n")
    elif "ping" in message.text:
        ping_device()
    elif "reboot" in message.text:
        wifi_reboot()
    else:
        bot.send_message(message.from_user.id, "Я тебя не понимаю. Напиши /help.")
bot.polling(none_stop=True, interval=0)
#####################################################################################################